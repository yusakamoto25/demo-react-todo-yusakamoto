import React, {useEffect, useState} from 'react';
import './App.css';
import Header from './componets/Header';
import Form from './componets/From'
import ToDoList from './componets/ToDoList'
import axios from 'axios';
import Modal from './componets/Modal';


const App = () => {



    // ToDoリストのStateをtoDoListと定義    
    const [toDoList, setToDoList] = useState([]);
        useEffect(() => {
        axios.get("http://localhost:8080/todos").then((res) => {
        const data = res.data.todos.map((todos) => ({
            id: todos.id,
            title: todos.title,
            detail: todos.detail,
        }))

        setToDoList(data);

        },);
    },[])

    // toDoListに項目を追加
    const addToDoList = (Title, Detail) => {
        setToDoList(toDoList.concat({"title": Title, "detail": Detail}));
        
    }

     // toDoListの項目を削除
    const deleteToDoList = (id) => { 
        // console.log(id)
        axios.delete(`http://localhost:8080/todos/${id}`).then(()=> {
            setToDoList(toDoList.filter(toDoList => toDoList.id !== id));
    })
    }

    










    return (
        <div className="toDo-app">
            <Header headerTitle="ToDoリスト"/>

            <div className="toDo-app-body container">
                <div className="toDo-main">
                    <Form add={addToDoList}/>
                    <ToDoList list={toDoList} delete={deleteToDoList} />
                    <Modal />                    
                                    

                </div>
            </div>
        </div>
    );
}

export default App;