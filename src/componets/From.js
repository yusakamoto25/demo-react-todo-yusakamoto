import axios from 'axios';
import React, {useState} from 'react';
import './Form.css'

const Form = (props) => {
    const [toDoTitle, setToDoTitle] = useState("");
    const [toDoContent, setToDoContent] = useState("");

    // Titleフォームの状態の制御
    const handleToDoTitleInputChange = (e) => {
        setToDoTitle(e.target.value);
    }

    // Contentフォームの状態の制御
    const handleToDoContentInputChange = (e) => {
        setToDoContent(e.target.value);
    }

    // 入力フォームのクリア
    const resetInputField = () => {
        setToDoTitle("");
        setToDoContent("");
    }

    // 項目の追加を確定
    const callAddToDoList = (e) => {
        const data = {
            title : toDoTitle,
            detail : toDoContent,
            date : "2021-11-11"
        }
        axios.post("http://localhost:8080/todos", data).then(()=> {
        e.preventDefault();
        console.log(data)
        props.add(toDoTitle,toDoContent);
        resetInputField();
    })
}

    return (
        <form className="toDo-form">
            <div className="toDo-form-title input-group"> 
                <div className="input-group-prepend">
                    <span className="input-group-text">Title</span>
                </div>
                <input type="text"
                    className="form-control shadow"
                    onChange={handleToDoTitleInputChange}
                    value={toDoTitle}
                />
            </div>
            <div className="toDo-form-detail input-group">
                <div className="input-group-prepend">
                    <span className="input-group-text">Detail</span>
                </div>
                <textarea className="form-control shadow"
                    aria-label="Detail"
                    onChange={handleToDoContentInputChange}
                    value={toDoContent}
                ></textarea>
            </div>
            <div className="toDo-form-add">
                <button className="btn btn-success"
                    type="submit" onClick={callAddToDoList}
                >
                    ADD
                </button>
            </div>
        </form>
    );
}

export default Form;