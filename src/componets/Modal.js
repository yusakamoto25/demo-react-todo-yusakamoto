
import axios from 'axios';
import React, { useState } from 'react';
import styled from "styled-components";

const Modal = ({show, setShow, info}) => {

    const [editTilte, setEditTitle] = useState("")
    const [editDetail, setEditDetail] = useState("")
    
    // Titleフォームの状態の制御
    const handleEditTitleInputChange = (e) => {
        setEditTitle(e.target.value);
    }

    // Contentフォームの状態の制御
    const handleEditDetailInputChange = (e) => {
        setEditDetail(e.target.value);
    }

    // 入力フォームのクリア
    const resetInputField = () => {
        setEditTitle("");
        setEditDetail("");
    }

    // 変更確定
    const callEditList = () => {
        const data = {
            title : editTilte,
            detail : editDetail,
            date: "2021-11-11"
        }
        // e.preventDefault();
        axios.put(`http://localhost:8080/todos/${info}`, data)
        console.log(info)
        resetInputField();
    }

    // 確定ボタン押した際の関数
    const handleChange = (e) => {
        setShow(false)
        callEditList(info)
    }

    

    if(show){
    return (

        <div>
            <SOverlay onClick={() => setShow(false)}>
            <SContent onClick={(e) => e.stopPropagation()}>
                <span className="input-group-addon" id="basic-addon1"></span>
                    <input type="text" className="form-control" placeholder="タイトル" aria-label="ユーザー名" aria-describedby="basic-addon1" onChange={handleEditTitleInputChange} value={editTilte}></input>
                <br></br>
                <span className="input-group-addon" id="basic-addon1"></span>
                    <input type="text" className="form-control" placeholder="内容" aria-label="ユーザー名" aria-describedby="basic-addon1" onChange={handleEditDetailInputChange} value={editDetail}></input>
                <p><button　className="btn btn-success" onClick={() => handleChange()}>確定</button></p>
                <p><button  className="btn btn-danger"　onClick={() => setShow(false)}>戻る</button></p>
            </SContent>
            </SOverlay>
        </div>
    
    )
    } else {
        return false;
    }


}

const SEditForm = styled.div`
    
    
    width: 100%;
    text-align: center;

`

const SOverlay = styled.div`

    position: fixed;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background-color:rgba(0,0,0,0.5);

    display: flex;
    align-items: center;
    justify-content: center;
`;

const SContent = styled.div`
    z-index:2;
    width:80%;
    padding: 1em;
    background:#fff;
    `
 export default Modal;