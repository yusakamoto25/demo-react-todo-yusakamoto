import React, { useState } from 'react';
import './ToDoList.css'
import Modal from './Modal';

const ToDoList = (props) => {
        // モーダル
    const [show, setShow] = useState(false)


    // AppコンポーネントのStateであるtoDoListをpropsとして受け取って
    // mapでループする
    const toDoListItems = props.list.map(
        (item, id) => {
            return (
                <div key={id} className="card toDo-item">
                    <div className="card-body">
                        <h5 className="card-title">Title: {item.title}</h5>
                        <p className="card-text">detail: {item.detail}</p>
                        <button className="btn btn-danger"
                            onClick={() => {props.delete(item.id); console.log(item.id)}} >
                            削除
                        </button>
                            <button className="btn btn-info"
                            onClick={() => {setShow(true); console.log(item.id)}} >
                            変更
                        </button>
                        <Modal show={show} setShow={setShow} info={item.id}/>    
                    </div>
                </div>
            );
        }
    );
    return (
        <div className="toDo-List">
            <h1>Your Tasks: {props.list.length}</h1>
            <div>
                {toDoListItems}

            </div>
        </div>
    );
}
export default ToDoList;